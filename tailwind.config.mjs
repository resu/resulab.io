/** @type {import('tailwindcss').Config} */
export default {
  content: ['./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}'],
  theme: {
    fontFamily: {
      sans: ['Open Sans'],
      mono: ['Roboto Mono'],
    },
    extend: {
      colors: {
        'fore-2': "hsl(var(--fore-2) / <alpha-value>)",
        'fore-1': "hsl(var(--fore-1) / <alpha-value>)",
        'fore-0': "hsl(var(--fore-0) / <alpha-value>)",
        'semi-2': "hsl(var(--semi-2) / <alpha-value>)",
        'semi-1': "hsl(var(--semi-1) / <alpha-value>)",
        'semi-0': "hsl(var(--semi-0) / <alpha-value>)",
        'back-3': "hsl(var(--back-3) / <alpha-value>)",
        'back-2': "hsl(var(--back-2) / <alpha-value>)",
        'back-1': "hsl(var(--back-1) / <alpha-value>)",
        'back-0': "hsl(var(--back-0) / <alpha-value>)",
      },
    },
  },
  plugins: [],
}
